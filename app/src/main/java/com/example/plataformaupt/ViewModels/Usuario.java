package com.example.plataformaupt.ViewModels;

public class Usuario {

        public String usuariold;
        public String titulo;
        public String descipcion;

    public String getUsuariold() {
        return usuariold;
    }

    public void setUsuariold(String usuariold) {
        this.usuariold = usuariold;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }
}
