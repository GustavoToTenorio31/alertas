package com.example.plataformaupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.plataformaupt.Api.Api;
import com.example.plataformaupt.Api.Servicios.ServicioPeticion;
import com.example.plataformaupt.ViewModels.PeticionLogin;
import com.example.plataformaupt.ViewModels.Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Descripcion extends AppCompatActivity {
    private String APITOKEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion);
        final SharedPreferences preferencia = getSharedPreferences("notificacionss", Context.MODE_PRIVATE);
        String validacion = preferencia.getString("NUEVO", "");
        final EditText iden = findViewById(R.id.idusu);
        final EditText tit = findViewById(R.id.titulo);
        final EditText descri = findViewById(R.id.desusu);
        Button botonenviar = (Button) findViewById(R.id.btndes);
        botonenviar.setOnClickListener(new View.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View v) {
                if (iden.getText().toString().isEmpty() || iden.getText().toString() == "") {
                    iden.setSelectAllOnFocus(true);
                    iden.requestFocus();
                    return;
                }
                if (tit.getText().toString().isEmpty() || tit.getText().toString() == "") {
                    tit.setSelectAllOnFocus(true);
                    tit.requestFocus();
                    return;
                }
                if (descri.getText().toString().isEmpty() || descri.getText().toString() == "") {
                    descri.setSelectAllOnFocus(true);
                    descri.requestFocus();
                    return;
                }
                ServicioPeticion service = Api.getApi(Descripcion.this).create(ServicioPeticion.class);
                Call<PeticionLogin> loginCall = service.getLogin(iden.getText().toString(), tit.getText().toString());
                loginCall.enqueue(new Callback<Usuario>() {
                    @Override
                    public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                        Usuario peticion = response.body();
                        if (peticion.usuariold == "true") {
                            APITOKEN = peticion.descipcion;
                            onSaveInstanceState(preferencia);
                            Toast.makeText(Descripcion.this, "Bienvenido", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Descripcion.this, Menu.class));
                        } else {
                            Toast.makeText(Descripcion.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Usuario> call, Throwable t) {

                    }


                });
            }

        });
    }
}
